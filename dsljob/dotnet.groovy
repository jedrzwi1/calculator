job ('job example for npm')
{
scm {
git('https://gitlab.com/jedrzwi1/calculator.git') { node ->
node / gitConfigName('DSL script')
node / gitConfigEmail('jenkins-dsl-script@altkom.com')
}
}



triggers {
scm('H/5 * * * *')
}





steps {
    msbuild {
        msBuildFile('Calculator.csproj')
        msBuildName('Calculator')
        cmdLineArgs('')
        buildVariablesAsProperties('')
        continueOnBuildFailure()
        unstableIfWarnings()
        doNotUseChcpCommand()
        }
    }
}