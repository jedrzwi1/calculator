﻿using System;

namespace Calculator
{
    class Program
    {
        static Engine en = new Engine();
        static void Main(string[] args)
        {
            Menu();
        }

        static void Menu() 
        {
            while(true)
            {
                Console.WriteLine("Wybierz dzialanie:");
                Console.WriteLine("1: dodawanie");
                Console.WriteLine("2: odejmowanie");
                Console.WriteLine("3: mnozenie");
                //Console.WriteLine("Wybierz działanie:");
                string dzialanie = Console.ReadLine();
                Console.WriteLine("Podaj a i b:");
                string a = Console.ReadLine();
                string b = Console.ReadLine();
                switch(dzialanie)
                {
                    case "1": Console.WriteLine("Wynik to:" + en.Add(int.Parse(a), int.Parse(b)));break;
                    case "2": Console.WriteLine("Wynik to:" + en.Substract(int.Parse(a), int.Parse(b)));break;
                    case "3": Console.WriteLine("Wynik to:" + en.Multiply(int.Parse(a), int.Parse(b)));break;
                }
            }
        }
    }
}
